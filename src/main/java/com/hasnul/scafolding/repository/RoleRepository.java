package com.hasnul.scafolding.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.hasnul.scafolding.model.Role;

public interface RoleRepository extends MongoRepository<Role, String>{
    @Query(value = "{}", fields = "{ 'id': 1, 'name': 1 }")
    List<Role> get();
} 


    

