package com.hasnul.scafolding.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.hasnul.scafolding.model.User;

@Repository
public interface UserRepository extends 
    MongoRepository<User, String>{   

    @Query(value = "{}", fields = "{ 'password': 0}")
    Page<User> findByNameLikeOrEmailLike(String name, String email, Pageable pageable);

    @Query(value = "{}", fields = "{ 'password': 0}")
    Page<User> findByNameOrEmail(String name, String email, Pageable pageable);

    @Query(value = "{}", fields = "{ 'password': 0}")
    Page<User> findAllByNameOrEmailOrderByNameAsc(String name, String email, Pageable pageable);

    //Page<User> findByNameOrEmailOrderByNameDesc(String name, String email, Pageable pageable);

    
} 


    

