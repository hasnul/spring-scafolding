package com.hasnul.scafolding.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.hasnul.scafolding.model.Book;

public interface BookRepository extends MongoRepository<Book, String>{

} 


    

