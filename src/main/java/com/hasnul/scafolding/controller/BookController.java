package com.hasnul.scafolding.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hasnul.scafolding.model.Book;
import com.hasnul.scafolding.repository.BookRepository;

@RequestMapping("/api/books")
@RestController
public class BookController {

    @Autowired
    private BookRepository repo;
    
    @GetMapping("")
    public List<Book>  table() {
        return repo.findAll();
    }

    @GetMapping("/id")
    public Book  id(@PathVariable String id) {
        return repo.findById(id).orElse(null);
    }

    @PostMapping("")
    public Book add(@RequestBody Book data) {
        return repo.save(data);
    }

    @PutMapping("/{id}")
    public Book updateBook(@PathVariable String id, @RequestBody Book data) {
        data.setId(id);
        return repo.save(data);
    }

    @DeleteMapping("/{id}")
    public void deleteBook(@PathVariable String id) {
        repo.deleteById(id);
    }


}
