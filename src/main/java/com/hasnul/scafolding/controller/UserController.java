package com.hasnul.scafolding.controller;
import java.util.HashMap;
import java.util.List;

import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
// import org.springframework.data.mongodb.core.query.Field;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// import com.hasnul.scafolding.model.Permission;
// import com.hasnul.scafolding.model.Role;
 import com.hasnul.scafolding.model.User;
// import com.hasnul.scafolding.repository.PermissionRepository;
// import com.hasnul.scafolding.repository.RoleRepository;
// import com.hasnul.scafolding.repository.UserRepository;
import com.hasnul.scafolding.repository.UserRepository;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

// TODO: 
// Pagination
// Belongsto
// Search from relation
// Task scheduler
// Background service
// Query attribute
// Validation
// Middleware Authentication
// Middleware Authorization
// Sso USU
// Swagger
// Testing
// Deploy Ubuntu VPS

@RequestMapping("/api/user")
@RestController
public class UserController {

    private final MongoTemplate mongoTemplate;


    public UserController(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
    
     @Autowired
     private UserRepository userRepository;
    // @Autowired
    // private RoleRepository roleRepository;
    // @Autowired
    // private PermissionRepository permissionRepository;
    
    @GetMapping(value="")
    @ResponseBody
    public ResponseEntity<Object> read (@RequestParam(defaultValue = "") String search, @RequestParam(defaultValue = "1") int page_number, @RequestParam(defaultValue = "10") int page_size) {
        try {
            // if(req == "single") {

            // }
        
         Criteria criteria = new Criteria();
         criteria.orOperator(Criteria.where("name").regex(".*" + search + ".*"), Criteria.where("email").regex(".*" + search + ".*"));
         Query query = new Query().addCriteria(criteria).with(PageRequest.of((int)page_number, (int)page_size).withSort(Direction.ASC, "name"));
          query.fields().exclude("password", "roles");
         List<User> data = mongoTemplate.find(query, User.class);
         HashMap<String, Object> result = new HashMap<String, Object>();
         result.put("data", data);
         result.put("page_number", page_number);
         result.put("page_size", page_size);
         return ResponseEntity.ok(result);
         
            
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("Internal Server Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value="/table")
    public Page<User> table(
        @RequestParam(required = false) String name, 
        @RequestParam(required = false) String email, 
        @RequestParam(defaultValue = "0") int page, 
        @RequestParam(defaultValue = "10") int size 
    ) {
        //pageable.setMaxPageSize(page_size);
        PageRequest pageRequest = PageRequest.of(page, size);
        //Page<User> result = userRepository.findByNameLikeOrEmailLike("%" + name + "%", email, pageRequest );
        Page<User> result = userRepository.findAllByNameOrEmailOrderByNameAsc("%" + name + "%", "%" + email + "%", pageRequest );
        return result;
    }

    
    
}
