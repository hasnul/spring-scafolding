package com.hasnul.scafolding.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hasnul.scafolding.model.Permission;
import com.hasnul.scafolding.model.Role;
import com.hasnul.scafolding.model.User;
import com.hasnul.scafolding.repository.PermissionRepository;
import com.hasnul.scafolding.repository.RoleRepository;
import com.hasnul.scafolding.repository.UserRepository;

@RequestMapping("/api/faker")
@RestController
public class FakerController {

    private final MongoTemplate mongoTemplate;

    public FakerController(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }
    
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PermissionRepository permissionRepository;
    
    @GetMapping("/user")
    public Object generate() {
        userRepository.deleteAll();
        permissionRepository.deleteAll();
        roleRepository.deleteAll();

        List<Permission> permissions = Arrays.asList(
            permissionRepository.save(new Permission(null, "sudo")),
            permissionRepository.save(new Permission(null, "user")),
            permissionRepository.save(new Permission(null, "user/write")),
            permissionRepository.save(new Permission(null, "role")),
            permissionRepository.save(new Permission(null, "role/write")),
            permissionRepository.save(new Permission(null, "permission")),
            permissionRepository.save(new Permission(null, "permission/write"))
        );

        List<Role> roles = Arrays.asList(
            roleRepository.save(new Role(null, "super administrator", permissions)),
            roleRepository.save(new Role(null, "administrator", Arrays.asList(permissions.get(0)))),
            roleRepository.save(new Role(null, "operator", Arrays.asList(permissions.get(1), permissions.get(2))))
        );
        List<User> users = new ArrayList<User>();
        
        for(int i=0; i<1000; i++) {
            users.add(new User("hasnul" + i, "hasnul" + i+ "@usu.ac.id", "12345", Arrays.asList(roles.get(0))));
            users.add(new User("feisal" + i, "feisal" + i+ "@usu.ac.id", "12345", Arrays.asList(roles.get(1))));
            users.add(new User("fauzi " + i, "fauzi" + i+ "@usu.ac.id", "12345", Arrays.asList(roles.get(2))));
        
        }       

        for (int i = 0; i < users.size(); i++) {
            User user = userRepository.save(users.get(i));
            System.out.println(user);
        }

        //users.stream().map(obj -> userRepository.save(obj));

        return ResponseEntity.ok("user generated " + users.size());
    }

    @GetMapping("/generate2")
    public Object generate2() {
        userRepository.deleteAll();
        permissionRepository.deleteAll();
        roleRepository.deleteAll();

        List<Permission> permissions = Arrays.asList(
            mongoTemplate.save(new Permission(null, "sudo")),
            mongoTemplate.save(new Permission(null, "user")),
            mongoTemplate.save(new Permission(null, "user/write")),
            mongoTemplate.save(new Permission(null, "role")),
            mongoTemplate.save(new Permission(null, "role/write")),
            mongoTemplate.save(new Permission(null, "permission")),
            mongoTemplate.save(new Permission(null, "permission/write"))
        );

        List<Role> roles = Arrays.asList(
            mongoTemplate.save(new Role(null, "super administrator", permissions)),
            mongoTemplate.save(new Role(null, "administrator", Arrays.asList(permissions.get(0)))),
            mongoTemplate.save(new Role(null, "operator", Arrays.asList(permissions.get(1), permissions.get(2))))
        );

        List<User> users = Arrays.asList( 
            new User("hasnul", "hasnul@usu.ac.id", "12345", Arrays.asList(roles.get(0))),
            new User("feisal", "feisal@usu.ac.id", "12345", Arrays.asList(roles.get(1))),
            new User("fauzi", "fauzi@usu.ac.id", "12345", Arrays.asList(roles.get(2)))
        );

        for (int i = 0; i < users.size(); i++) {
            User user = mongoTemplate.save(users.get(i));
            System.out.println(user);
        }

        //users.stream().map(obj -> userRepository.save(obj));

        return "oke2";
    }    
}
