package com.hasnul.scafolding.controller.usu;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api/usu/user")
@RestController
public class UserController {
     
    @GetMapping(value="/sync")
    public void sync() {

    }

    @GetMapping(value="/search")
    public void search() {

    }
}
