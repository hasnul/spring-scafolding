package com.hasnul.scafolding;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

import com.hasnul.scafolding.model.User;
import com.hasnul.scafolding.repository.UserRepository;

@EnableMongoAuditing
@SpringBootApplication
public class ScafoldingApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScafoldingApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(UserRepository repository) {
		return args -> {
			User user = new User("marthin", "marthin@yahoo.com", "12345", null);
		};
	}

}
