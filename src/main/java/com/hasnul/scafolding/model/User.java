package com.hasnul.scafolding.model;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.web.bind.annotation.ModelAttribute;

import lombok.Data;

@Document(collection =  "users")
@Data
public class User  {
    @Id
    public String id;

    private String name;
    private String email;
    private String password;

    
    @DBRef
    private List<Role> roles;

    @CreatedDate
    @Field(name = "created_at")
    public LocalDateTime createdAt;

    @LastModifiedDate
    @Field(name = "updated_at")
    public LocalDateTime updatedAt;

    public User(String name, String email, String password, List<Role> roles) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.roles = roles;
    }

    //private String fullName;
    public String getFullName() {
        return this.name + '|' + this.email;
    }

}

