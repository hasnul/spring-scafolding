package com.hasnul.scafolding.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;

@Document(collection =  "permissions")
@Data
@AllArgsConstructor
public class Permission{
    @Id
    private String id;
    private String name;

}

