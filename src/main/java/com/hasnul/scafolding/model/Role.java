package com.hasnul.scafolding.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;

@Document(collection =  "roles")
@Data
@AllArgsConstructor
public class Role {
    @Id
    private String id;
    private String name;

    @DBRef
    private List<Permission> permissions;
}

