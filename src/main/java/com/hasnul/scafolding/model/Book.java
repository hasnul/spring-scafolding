package com.hasnul.scafolding.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;

@Document(collection =  "books")
@Data
@AllArgsConstructor
public class Book {
    @Id
    private String id;
    private String title;
    private String author;
    
}
